package io.gitlab.hjoeren.experimental;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VsApp implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(VsApp.class, args);
	}
	
	@Value("${de.ednpoint}")
	private String deEdnpoint;

	@Override
	public void run(String... args) throws Exception {
		System.out.println(deEdnpoint);
	}

}
